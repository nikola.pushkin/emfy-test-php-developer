<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=windows-1251">
    <link href="css/select2.min.css" rel="stylesheet"/>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".test-select").select2();
        });
    </script>
</head>
<body>
<?php

ini_set('default_charset', 'windows-1251');
echo "<p>Тестовый комментарий</p>";
require_once("TopDelivery.php");
const TYPE_ID = "id";
const TYPE_STRING = "string";
const TYPE_ZIP = "zip";
$topDelievery = new TopDelivery();
$list = $topDelievery->getCitiesRegions();
$searchTitle = "Санкт-Петербург";
$searchTitleUtf = mb_convert_encoding($searchTitle, "UTF-8", "cp1251");

echo "Поиск id города '" . $searchTitle . "' вариант 2: ";
$searchKey = array_search($searchTitleUtf, array_column($list, "cityName"));
$stPetersburgId = $list[$searchKey]["cityId"];
echo " ID=" . $stPetersburgId . "<br>\n";

$intervalsRawResponse = $topDelievery->getNearDeliveryDatesIntervals(TYPE_ID, $stPetersburgId);
echo "Доставки в городе " . $searchTitle . ":<br>\n";
echo '<select class="test-select" name="date">';
foreach ($intervalsRawResponse->dateTimeIntervals as $key => $interval) {
    echo "<option value='$interval->date'>" . $interval->date . "</option>\n";
}
echo "</select>";
?>
</body>
</html>
