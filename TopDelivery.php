<?php

class TopDelivery
{
      private $login = 'webshop';
    private $password = 'pass';
    private $Url = 'http://is-test.topdelivery.ru/api/soap/w/2.0/?wsdl';
    private $Login = 'tdsoap';
    private $Password = '5f3b5023270883afb9ead456c8985ba8';

    private function getClient()
    {
        return new SoapClient(
            $this->Url,
            array(
                'login' => $this->Login,
                'password' => $this->Password
            )
        );
    }

    function getCitiesRegions()
    {

        $client = self::getClient();
        $params['auth'] = new stdClass;
        $params['auth']->login = $this->login;
        $params['auth']->password = $this->password;

        $request['getCitiesRegions'] = $params;
        $result = $client->__soapCall('getCitiesRegions', $request);

        if ($result->requestResult->status == 0) {
            $table = array();
            foreach ($result->citiesRegions as $region) {
                foreach ($region->cities as $city) {
                    if (isset($city->cityId) && isset($city->cityName))
                        $table[] = array(
                            'cityId' => $city->cityId,
                            'cityName' => $city->cityName,
                            'regionId' => $region->regionId,
                            'regionName' => $region->regionName,
                        );
                }
            }
            return $table;
        }

        return array();
    }


    /**
     * Добавляем 2 параметра для поиска по городам
     * @param $type - тип из списка id, string, zip - из документации поиск по id, названию или zip-коду соответственно
     * @param $city - параметр поиска (здесь должен быть id, название города или его код)
     * @return mixed
     */
    function getNearDeliveryDatesIntervals($type, $city)
    {
        $client = self::getClient();
        $params['auth'] = new stdClass;
        $params['auth']->login = $this->login;
        $params['auth']->password = $this->password;


        $params['addressDeliveryProperties'] = array(
            'serviceType' => 'DELIVERY',
            'deliveryType' => 'COURIER',
            'orderSubtype' => 'SIMPLE',
            'deliveryAddress' => array("city" => $city, "type" => $type)
        );

        $request['getNearDeliveryDatesIntervals'] = $params;
        $result = $client->__soapCall('getNearDeliveryDatesIntervals', $request);

        return $result;
    }
}

